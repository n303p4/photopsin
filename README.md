# Photopsin

A Folding@home (FAH) stats panel written in Python.

## Disclaimer

I originally wrote Photopsin as a giant hack to cover my own use case. To say that the code is "sloppy" would be putting it lightly. There may be a lot of bugs and other problems that make it unsuitable for your use case.

If you want to contribute in some way regardless, merge requests are welcome.

## Quickstart

On a Folding@home machine running Ubuntu, Debian, or Fedora, simply run the script `install.sh`. This will copy the program to `/opt`, and also install a systemd service and start the web server.

Your Folding@home machine may have a firewall on by default (Fedora uses firewalld). In this case, you will have to open port `80/tcp`.

You can then view stats by pointing your browser to the IP address of your folding@home machine.

## Enable folding.extremeoverclocking.com (EOC) links

Edit the line in `photopsin.service`:

```bash
ExecStart=/bin/bash -c "sleep 10s && python3 main.py"
```

to:

```bash
ExecStart=/bin/bash -c "sleep 10s && PHOTOPSIN_EOC_ID=<id> python3 main.py"
```

where `<id>` is the user ID on EOC (different from your normal FAH user ID).

This will change the donor link to point to EOC, and add an EOC team link next to the normal team link.
