#!/bin/bash
if [[ $(id -u) -ne 0 ]]; then
    echo "Please run this script as root or sudo."
    exit 1
fi
if grep "Fedora" /etc/os-release >> /dev/null; then
    echo "Installing dependencies for Fedora ..."
    dnf -y install python3-cheroot python3-flask python3-psutil python3-py3nvml python3-requests
elif grep "Ubuntu" /etc/os-release >> /dev/null || grep "Debian" /etc/os-release >> /dev/null; then
    echo "Installing dependencies for Ubuntu/Debian-based OS ..."
    apt-get -y install python3-cheroot python3-flask python3-psutil python3-pynvml python3-requests
else
    echo "Unknown operating system; not installing dependencies."
fi
if [ -d "/opt/photopsin" ]; then
    echo "Removing existing directory /opt/photopsin ..."
    rm -rf /opt/photopsin
fi
echo "Copying photopsin to /opt/photopsin ..."
cp -r . /opt/photopsin
rm -rf /opt/photopsin/.git
rm -rf /opt/photopsin/.gitignore
if [ -f "/etc/systemd/system/photopsin.service" ]; then
    echo "Restarting photopsin systemd service ..."
    systemctl restart photopsin
else
    echo "Installing and enabling photopsin systemd service ..."
    cp ./photopsin.service /etc/systemd/system
    systemctl enable --now photopsin
fi
