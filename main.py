"""Start photopsin under a Cheroot server."""

import argparse

from cheroot.wsgi import Server
from photopsin import create_app


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("-l", "--local-stats-ratelimit", metavar="SECONDS", type=float, default=9,
                        help="Rate limit for fetching local stats.")
    parser.add_argument("-w", "--web-stats-ratelimit", metavar="SECONDS", type=float, default=599,
                        help="Rate limit for fetching web stats.")
    parser.add_argument("-i", "--project-info-ratelimit", metavar="SECONDS", type=float, default=0.25,
                        help="Rate limit for fetching project info.")
    parser.add_argument("--host", metavar="NAME", type=str, default="0.0.0.0",
                        help="Set host/IP address to run the server on.")
    parser.add_argument("-p", "--port", metavar="NUMBER", type=int, default=80,
                        help="Set port number.")
    args = parser.parse_args()

    app = create_app(local_stats_ratelimit=args.local_stats_ratelimit,
                     web_stats_ratelimit=args.web_stats_ratelimit,
                     project_info_ratelimit=args.project_info_ratelimit)
    server = Server(bind_addr=(args.host, args.port), wsgi_app=app)
    try:
        server.start()
    finally:
        server.stop()
