"""Photopsin, a folding@home stats display in Python."""

from datetime import datetime
import json
import os
from traceback import print_exc

from flask import Flask, jsonify, render_template

from . import api


def get_local_stats(cpu_model: str = None):
    """Get local stats from FAHClient and other sources."""

    try:
        local_stats = api.local.get_stats(cpu_model=cpu_model)
        passkey = local_stats.get("passkey", None)
        if "passkey" in local_stats:
            del local_stats["passkey"]
        local_stats["eoc_id"] = os.environ.get("PHOTOPSIN_EOC_ID")
        return local_stats, passkey
    except Exception:
        print_exc()
        return {}, None


def get_web_stats(local_stats: dict, passkey: str):
    """Get web stats from the folding@home web API."""

    donor_name = local_stats.get("donor")
    if not donor_name:
        return
    try:
        return {
            "user": api.web.get_user_stats(donor_name),
            "bonus": api.web.get_bonus_stats(donor_name, passkey),
            "eoc_id": os.environ.get("PHOTOPSIN_EOC_ID"),
            "time_updated": datetime.utcnow()
        }
    except Exception:
        print_exc()


def commas(number: int):
    if str(number).isdecimal():
        number = int(str(number))
    if isinstance(number, int):
        return f"{number:,}"
    return None


def flatten_stats(local_stats: dict):
    now = datetime.utcnow()
    folding_slots = []
    for slot_info in local_stats.get("slot_info", []):
        queue_info = slot_info.get("queue_info", {})
        simulation_info = slot_info.get("simulation_info", {})
        hardware_info = slot_info.get("hardware_info", {})

        donor_desc = []
        slot_id = slot_info.get("id")
        if slot_id:
            donor_desc.append(f"Slot {slot_id} of")
        donor = local_stats.get("donor")
        if donor:
            donor_desc.append(donor)

        description = slot_info.get("description")
        if not hardware_info and "gpu" in description:
            description = "GPU is lost; system reboot required!"

        project = simulation_info.get("project")
        run = simulation_info.get("run")
        clone = simulation_info.get("clone")
        gen = simulation_info.get("gen")
        has_prcg = any((project, run, clone, gen))
        if has_prcg:
            prcg = (
                f"{project} ({run}, {clone}, {gen})",
                f"https://stats.foldingathome.org/project/{project}"
            )
        else:
            prcg = None

        percentdone = queue_info.get("percentdone") if has_prcg else None
        ppd = commas(queue_info.get("ppd"))
        if ppd == "0":
            ppd = None
        tpf = queue_info.get("tpf")

        deadline = queue_info.get("deadline")
        if deadline:
            try:
                deadline = datetime.fromisoformat(deadline.rstrip("Z")) - now
                deadline = str(deadline).rsplit(":", 1)[0]
            except Exception:
                pass

        utilization = hardware_info.get("utilization")
        if isinstance(utilization, (int, float)):
            utilization = f"{utilization}%"
        load_average = " ".join("{:.2f}".format(l) for l in hardware_info.get("load_average", []))
        clock = hardware_info.get("clock")
        if isinstance(clock, list):
            clock = [
                f"{int(round(sum(clock) / len(clock)))}MHz",
                "\n".join("Processor {0}: {1:.3f}MHz".format(i, c) for i, c in enumerate(clock))
            ]
            if load_average:
                clock[1] = f"Load average: {load_average}\n{clock[1]}"
        if isinstance(clock, (int, float)):
            clock = f"{int(round(clock))}MHz"
        memory_usage = hardware_info.get("memory_usage")
        memory_total = hardware_info.get("memory_total")
        if isinstance(memory_usage, int):
            memory_usage = f"{memory_usage}MiB ({memory_usage * 100 // (memory_total or 1)}%)"
        if isinstance(memory_total, int):
            memory_total = f"{memory_total}MiB"
        fan_speed = hardware_info.get("fan_speed")
        temperature = hardware_info.get("temperature", "")
        if isinstance(temperature, list):
            temperature = [f"{temperature[0][1]}°C", "\n".join(f"{t[0]}: {t[1]}°C" for t in temperature)]
        elif isinstance(temperature, int):
            temperature = f"{temperature}°C"
        power_usage = hardware_info.get("power_usage")
        power_usage = f"{power_usage // 1000}W" if power_usage else None
        power_limit = hardware_info.get("power_limit")
        power_limit = f"{power_limit // 1000}W" if power_limit else None

        folding_slots.append({
            "Slot": " ".join(donor_desc),
            "Description": description,
            "Work unit PRCG": prcg,
            "Core": simulation_info.get("core"),
            r"% done": percentdone,
            "Status": slot_info.get("status"),
            "PPD": ppd,
            "TPF": tpf,
            "ETA": queue_info.get("eta"),
            "Deadline": deadline,
            "Estimated credit": commas(queue_info.get("creditestimate")),
            "Base credit": commas(queue_info.get("basecredit")),
            "Utilization": utilization,
            "Clock": clock,
            "Memory usage": memory_usage,
            "Total memory": memory_total,
            "Fan speed": f"{fan_speed}%" if isinstance(fan_speed, int) else None,
            "Temperature": temperature,
            "Power usage": power_usage,
            "Power limit": power_limit
        })
    return folding_slots


def create_app(*, local_stats_ratelimit: int = 9, web_stats_ratelimit: int = 599):
    """Application factory."""

    app = Flask(__name__, static_folder="img", template_folder="templates")
    app.secret_key = os.urandom(64)

    app.config["local_stats_ratelimit"] = local_stats_ratelimit
    app.config["web_stats_ratelimit"] = web_stats_ratelimit
    app.config["local_stats_ratelimit_flag"] = datetime(1970, 1, 1)
    app.config["web_stats_ratelimit_flag"] = datetime(1970, 1, 1)
    app.config["cpu_model"] = api.local.get_cpu_model()
    app.config["local_stats"], app.config["passkey"] = get_local_stats(app.config["cpu_model"]) or {}
    app.config["web_stats"] = get_web_stats(app.config["local_stats"], app.config["passkey"]) or {}

    def _get_local_stats():
        now = datetime.utcnow()
        last_requested_ago = now - app.config["local_stats_ratelimit_flag"]
        if last_requested_ago.seconds >= app.config["local_stats_ratelimit"]:
            new_stats, passkey = get_local_stats(app.config["cpu_model"])
            if new_stats:
                app.config["local_stats"] = new_stats
            if passkey:
                app.config["passkey"] = passkey
            app.config["local_stats_ratelimit_flag"] = now

    @app.route("/")
    def index():
        """Index route. Displays all the shiny numbers."""
        return render_template("index.jinja")

    @app.route("/simple")
    def index_simple():
        """Index route, but it's simple."""
        _get_local_stats()
        return render_template("simple.jinja", stats=app.config["local_stats"])

    @app.route("/local-stats.json")
    def local_stats():
        """Local client stats."""
        _get_local_stats()
        return jsonify(app.config["local_stats"])

    @app.route("/project/<project_id>")
    def project_info(project_id):
        """Project info."""
        return jsonify(api.web.get_project_info(project_id))

    @app.route("/web-stats.json")
    def web_stats():
        """API stats for the folding user."""
        now = datetime.utcnow()
        last_requested_ago = now - app.config["web_stats_ratelimit_flag"]
        if last_requested_ago.seconds >= app.config["web_stats_ratelimit"]:
            new_stats = get_web_stats(app.config["local_stats"], app.config["passkey"])
            if new_stats:
                app.config["web_stats"] = new_stats
            app.config["web_stats_ratelimit_flag"] = now
        return jsonify(app.config["web_stats"])

    return app


if __name__ == "__main__":
    app = create_app()
    app.run()
