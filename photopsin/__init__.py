"""Photopsin, a folding@home stats display in Python."""

from datetime import datetime
import json
import os
from traceback import print_exc

from flask import Flask, abort, jsonify, render_template

from . import api


def get_local_stats(cpu_model: str = None):
    """Get local stats from FAHClient and other sources."""

    try:
        local_stats = api.local.get_stats(cpu_model=cpu_model)
        passkey = local_stats.get("passkey", None)
        if "passkey" in local_stats:
            del local_stats["passkey"]
        local_stats["eoc_id"] = os.environ.get("PHOTOPSIN_EOC_ID")
        return local_stats, passkey
    except Exception:
        print_exc()
        return {}, None


def get_web_stats(local_stats: dict, passkey: str):
    """Get web stats from the folding@home web API."""

    donor_name = local_stats.get("donor")
    if not donor_name:
        return
    try:
        return {
            "user": api.web.get_user_stats(donor_name),
            "bonus": api.web.get_bonus_stats(donor_name, passkey),
            "eoc_id": os.environ.get("PHOTOPSIN_EOC_ID"),
            "time_updated": datetime.utcnow()
        }
    except Exception:
        print_exc()


def create_app(*, local_stats_ratelimit: int = 9, web_stats_ratelimit: int = 599, project_info_ratelimit: int = 0.25):
    """Application factory."""

    app = Flask(__name__, static_folder="static", template_folder="templates")
    app.secret_key = os.urandom(64)

    app.config["local_stats_ratelimit"] = local_stats_ratelimit
    app.config["web_stats_ratelimit"] = web_stats_ratelimit
    app.config["project_info_ratelimit"] = project_info_ratelimit
    app.config["local_stats_ratelimit_flag"] = datetime(1970, 1, 1)
    app.config["web_stats_ratelimit_flag"] = datetime(1970, 1, 1)
    app.config["project_info_ratelimit_flag"] = datetime(1970, 1, 1)
    app.config["cpu_model"] = api.local.get_cpu_model()
    app.config["local_stats"], app.config["passkey"] = get_local_stats(app.config["cpu_model"]) or {}
    app.config["web_stats"] = get_web_stats(app.config["local_stats"], app.config["passkey"]) or {}

    @app.route("/")
    def index():
        """Index route. Displays all the shiny numbers."""
        return render_template("index.jinja")

    @app.route("/local-stats.json")
    def local_stats():
        """Local client stats."""
        now = datetime.utcnow()
        last_requested_ago = now - app.config["local_stats_ratelimit_flag"]
        if last_requested_ago.seconds >= app.config["local_stats_ratelimit"]:
            new_stats, passkey = get_local_stats(app.config["cpu_model"])
            if new_stats:
                app.config["local_stats"] = new_stats
            if passkey:
                app.config["passkey"] = passkey
            app.config["local_stats_ratelimit_flag"] = now
        return jsonify(app.config["local_stats"])

    @app.route("/project/<project_id>")
    def project_info(project_id):
        """Project info."""
        now = datetime.utcnow()
        last_requested_ago = now - app.config["project_info_ratelimit_flag"]
        if last_requested_ago.seconds < app.config["project_info_ratelimit"]:
            abort(429)
        return jsonify(api.web.get_project_info(project_id))

    @app.route("/web-stats.json")
    def web_stats():
        """API stats for the folding user."""
        now = datetime.utcnow()
        last_requested_ago = now - app.config["web_stats_ratelimit_flag"]
        if last_requested_ago.seconds >= app.config["web_stats_ratelimit"]:
            new_stats = get_web_stats(app.config["local_stats"], app.config["passkey"])
            if new_stats:
                app.config["web_stats"] = new_stats
            app.config["web_stats_ratelimit_flag"] = now
        return jsonify(app.config["web_stats"])

    return app


if __name__ == "__main__":
    app = create_app()
    app.run()
