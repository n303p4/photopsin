window.addEventListener("load", () => {

const workUnitEtas = {};
const workUnitProgressPerSecond = {};
const workUnitProgresses = {};
const projectInfoCache = {};
const slotProjects = {};
const descriptionsVisible = {};

let lastUpdated = "";
let lastUpdated2 = "";
let slotInfoContainerHeightSet = false;
if (browserIsWebKit()) {
    // WebKit is terrible lol
    let donorBoxLegendHeight = document.querySelector("#donor-box > legend").offsetHeight;
    document.getElementById("donor-box").style = `background-position: 0 ${-donorBoxLegendHeight}px; background-size: auto calc(100% + ${donorBoxLegendHeight}px);`;
}
getStats();
getAPIUserStats();
setInterval(updatePage, 1000);
setInterval(getAPIUserStats, 60000);

function hideElement(e) {
    e.classList.add("hidden");
}

function showElement(e) {
    e.classList.remove("hidden");
}

function average(array) {
    return array.reduce((a, b) => a + b) / array.length;
}

function updatePage() {
    if (Math.floor(Date.now() / 1000) % 10 === 0) {
        getStats();
    }
    else {
        updateProgress();
    }
    updateEta();
}

function updateEta() {
    for (let selector in workUnitEtas) {
        if (!workUnitEtas.hasOwnProperty(selector)) {
            continue;
        }
        if (typeof workUnitEtas[selector] !== "number") {
            continue;
        }
        if (workUnitEtas[selector] > 0) {
            workUnitEtas[selector] -= 1;
        }
        try {
            document.querySelector(selector).innerHTML = etaToHms(workUnitEtas[selector]) + "\xa0";
        }
        catch {}
    }
}

function updateProgress() {
    for (let selector in workUnitProgressPerSecond) {
        if (!workUnitProgresses.hasOwnProperty(selector) || !workUnitProgressPerSecond.hasOwnProperty(selector)) {
            continue;
        }
        if (typeof workUnitProgressPerSecond[selector] !== "number") {
            continue;
        }
        if (workUnitProgressPerSecond[selector] === Infinity) {
            continue;
        }
        workUnitProgresses[selector] = Math.min(100, workUnitProgresses[selector] + workUnitProgressPerSecond[selector]);
        let progressValue = (workUnitProgresses[selector]).toFixed(2);
        try {
            let bar = document.querySelector(selector);
            bar.style.width = `${progressValue}%`;
            bar.innerText = `\xa0${progressValue}%`;
        }
        catch {}
    }
}

function etaToHms(eta) {
    if (eta > 0) {
        let hours = (Math.floor(eta / 3600) + "").padStart(2, "0") + ":";
        let minutes = (Math.floor(eta % 3600 / 60) + "").padStart(2, "0") + ":";
        let seconds = (Math.floor(eta % 3600 % 60) + "").padStart(2, "0");
        return `ETA: ${hours}${minutes}${seconds}`;
    }
    else {
        return "ETA: --:--:--";
    }
}

function browserIsWebKit() {
    return navigator.userAgent.includes("WebKit");
}

function createDiv(innerHTML) {
    let div = document.createElement("div");
    div.innerHTML = innerHTML;
    return div;
}

function fahTimeToSeconds(timeString) {
    let substrings = timeString.split(" ");
    let seconds = 0;
    for (let i = 0; i < substrings.length; i++) {
        let substring = substrings[i];
        let substringFloat = parseFloat(substring);
        if (isNaN(substringFloat)) {
            switch (substring) {
                case "days":
                    seconds += substrings[i-1] * 24 * 60 * 60;
                case "hours":
                    seconds += substrings[i-1] * 24 * 60;
                case "mins":
                    seconds += substrings[i-1] * 60;
                default:
                    seconds += substrings[i-1];
            }
        }
        else {
            substrings[i] = substringFloat;
        }
    }
    return seconds;
}

function formatFAHTimeString(timeString) {
    let substrings = timeString.split(" ");
    for (let i = 0; i < substrings.length; i++) {
        let substring = substrings[i];
        let substringFloat = parseFloat(substring);
        if (isNaN(substringFloat)) {
            if (substrings[i-1] === 1) {
                substring = substring
                    .replace("hours", "hour")
                    .replace("mins", "minute")
                    .replace("secs", "second");
            }
            else {
                substring = substring
                    .replace("min", "minute")
                    .replace("sec", "second");
            }
            if (i < substrings.length - 1) {
                substring += ",";
            }
        }
        else {
            substring = substringFloat;
        }
        substrings[i] = substring;
    }
    return substrings.join(" ");
}

function formatFAHTimeStringShort(timeString) {
    if (!timeString) {
        return "";
    }
    let substrings = timeString.split(" ");
    for (let i = 0; i < substrings.length; i++) {
        let substring = substrings[i];
        let substringFloat = parseFloat(substring);
        if (isNaN(substringFloat)) {
            substring = substring
                .replace("days", "d")
                .replace("hours", "h")
                .replace("mins", "m")
                .replace("secs", "s");
            if (i < substrings.length - 1) {
                substring += " ";
            }
        }
        else {
            substring = substringFloat;
        }
        substrings[i] = substring;
    }
    return substrings.join("");
}

function createProgressBar(parent, percent, running=false, additionalText="") {
    let progress = document.createElement("div");
    progress.className = "progress";
    let bar = document.createElement("div");
    bar.className = "bar";
    if (running) {
        bar.classList.add("running");
    }
    bar.style.width = "0";
    bar.innerText = `\xa0${percent}${additionalText}`;
    progress.appendChild(bar);
    parent.appendChild(progress);
    setTimeout(() => {
        bar.style.width = `${percent}`
    }, 10);
    if (percent === "0.00%") {
        bar.classList.add("noborder");
    }
    return progress;
}

function createIfNotExists(parent, elementType, childSelector, content=null) {
    let child = parent.querySelector(childSelector);
    if (!child) {
        child = document.createElement(elementType);
        if (childSelector.startsWith("#")) {
            child.id = childSelector.replace("#", "");
        }
        if (childSelector.startsWith(".")) {
            child.className = childSelector.replace(".", "");
        }
        if (content) {
            child.innerHTML = content;
        }
        parent.appendChild(child);
    }
    return child;
}

function simplifyDescription(description) {
    if (description.includes("[") && description.includes("]")) {
        return description.substr(
            description.indexOf("[") + 1,
            description.indexOf("]") - description.indexOf("[") - 1
        ).replace("Lite Hash Rate", "LHR");
    }
    return description;
}

function createSlotInfoBox(slotInfoContainer, slot, nvml=false) {
    let slotInfoBoxId = `sib-${slot.id}`;
    let slotInfoBox = document.getElementById(slotInfoBoxId);
    let slotIsDisabled = slot.status === "DISABLED" || (slot.status === "PAUSED" && !slot.simulation_info && (!slot.queue_info || Object.keys(slot.queue_info).length === 0));
    if (!slot.queue_info) {
        slot.queue_info = {};
    }
    if (!slotInfoBox) {
        slotInfoBox = document.createElement("fieldset");
        slotInfoBox.id = slotInfoBoxId;
        slotInfoContainer.appendChild(slotInfoBox);
    }
    if (slotIsDisabled) {
        slotInfoBox.classList.add("disabled");
    }
    else {
        slotInfoBox.classList.remove("disabled");
    }

    let slotInfoDescription = slotInfoBox.querySelector("legend");
    if (!slotInfoDescription) {
        slotInfoDescription = createIfNotExists(slotInfoBox, "legend", "legend");
        if (slot.description.includes("gpu")) {
            slotInfoDescription.innerHTML = `<span class="slot-id">${slot.id}: </span>`;
            slotInfoDescription.innerHTML += `<span class="pci-bus-info">${slot.description.split(" ")[0]} </span>`;
            slotInfoDescription.innerHTML += `${simplifyDescription(slot.description)}`;
            slotInfoDescription.innerHTML += `<span class="gpu-die"> [${slot.description.split(" ")[1]}]</span>`
            slotInfoDescription.innerHTML = `<h2>${slotInfoDescription.innerHTML}</h2>`;
        }
        else {
            let description = slot.description.replace("(R)", "").replace("(TM)", "");
            slotInfoDescription.innerHTML = `<h2><span class="slot-id">${slot.id}: </span>${description}</h2>`;
        }
    }

    let workUnitHeader = createIfNotExists(slotInfoBox, "h3", "h3");
    if (slot.simulation_info && slot.simulation_info.project > 0) {
        workUnitHeader.innerHTML = `Project:<a id="slot-${slot.id}-link" title="Click for project details">${slot.simulation_info.project}</a>`;
        workUnitHeader.innerHTML += `<span class="rcg"> (Run ${slot.simulation_info.run}, Clone ${slot.simulation_info.clone}, Gen ${slot.simulation_info.gen})`
        workUnitHeader.innerHTML += `<span class="core"> [${slot.simulation_info.core||"None"}]</span></span>`;
    }
    else {
        workUnitHeader.innerHTML = "No project"
    }

    let projectDescription = createIfNotExists(slotInfoBox, "div", ".project-description");
    let slotProjectLink = document.getElementById(`slot-${slot.id}-link`);
    if (!descriptionsVisible[slot.id]) {
        projectDescription.classList.add("hidden");
    }
    else if (slotProjectLink && slot.simulation_info.project !== slotProjects[slot.id]) {
        slotProjects[slot.id] = slot.simulation_info.project;
        getProjectInfo(slot.simulation_info.project, projectDescription);
    }
    if (slotProjectLink) {
        slotProjectLink.onclick = () => {
            getProjectInfo(slot.simulation_info.project, projectDescription);
            projectDescription.classList.toggle("hidden");
            descriptionsVisible[slot.id] = !descriptionsVisible[slot.id];
        }
    }

    let workUnitProgress = slotInfoBox.querySelector(".progress");
    let percentDoneAsFloat = 0;
    try {
        percentDoneAsFloat = parseFloat(slot.queue_info.percentdone.replace("%", ""));
    }
    catch {}
    let yesUpdateProgress = !(workUnitProgresses[`#${slotInfoBoxId} > .progress > .bar`] === 100.00 && percentDoneAsFloat === 99.99);
    if (yesUpdateProgress) {
        workUnitProgresses[`#${slotInfoBoxId} > .progress > .bar`] = percentDoneAsFloat;
    }
    if (!workUnitProgress) {
        workUnitProgress = createProgressBar(slotInfoBox, slot.queue_info.percentdone || "0.00%", running=(slot.status === "RUNNING"));
        workUnitProgress.style = "margin-bottom: 0.5em;";
    }
    else if (yesUpdateProgress) {
        let bar = workUnitProgress.querySelector(".bar");
        bar.style.width = slot.queue_info.percentdone || "0.00%";
        if (slot.queue_info.percentdone === "0.00%") {
            bar.classList.add("noborder");
        }
        else {
            bar.classList.remove("noborder");
        }
        bar.innerText = `\xa0${slot.queue_info.percentdone || "0.00%"}`;
        bar.style.animation = "none";
        bar.offsetHeight;
        bar.style.animation = "";
        if (slot.status === "RUNNING") {
            bar.classList.add("running");
        }
        else {
            bar.classList.remove("running");
        }
    }

    let additionalSlotInfo = createIfNotExists(slotInfoBox, "div", ".additional-slot-info");

    let simulationInfoDiv = createIfNotExists(additionalSlotInfo, "div", ".simulation-info");

    if (slotIsDisabled) {
        additionalSlotInfo.style = "margin-top: 0.5em;";
        hideElement(workUnitHeader);
        hideElement(workUnitProgress);
    }
    else {
        additionalSlotInfo.style = "";
        showElement(workUnitHeader);
        showElement(workUnitProgress);
    }

    if (slotIsDisabled) {
        simulationInfoDiv.innerHTML = "<div class='disabled'>Not folding</div>";
        let rawState = slot.queue_info.state || slot.status;
        let readableState = `${rawState.charAt(0) + rawState.substr(1).toLowerCase()}`;
        if (slot.reason) {
            readableState += ` (${slot.reason})`;
        }
        simulationInfoDiv.innerHTML += `<div class='disabled'>Current state: ${readableState}</div>`;
    }
    else {
        let disabledDivs = simulationInfoDiv.querySelectorAll(".disabled");
        if (disabledDivs.length) {
            disabledDivs.forEach((e) => e.remove());
        }
        let workUnitPPDDiv = createIfNotExists(simulationInfoDiv, "div", ".ppd");
        let workUnitPPD = parseInt(`${slot.queue_info.ppd || 0}`).toLocaleString("en-US");
        workUnitPPDDiv.innerHTML = `<strong>${workUnitPPD}</strong> points per day`;

        let workUnitTPFAsSeconds = 0;
        try {
            workUnitTPFAsSeconds = fahTimeToSeconds(slot.queue_info.tpf);
        }
        catch {}
        let progressPerSecond = (100 / (slot.simulation_info.total_iterations || 100)) / workUnitTPFAsSeconds;
        if (slot.queue_info.state !== "RUNNING") {
            workUnitProgressPerSecond[`#${slotInfoBoxId} > .progress > .bar`] = slot.queue_info.state;
        }
        else {
            workUnitProgressPerSecond[`#${slotInfoBoxId} > .progress > .bar`] = progressPerSecond;
        }
        let workUnitFPD = 0;
        if (workUnitTPFAsSeconds > 0) {
            workUnitFPD = Math.round((24 * 60 * 60) / workUnitTPFAsSeconds).toLocaleString("en-US");
        }
        let workUnitTPF = "";
        try {
            workUnitTPF= formatFAHTimeStringShort(slot.queue_info.tpf);
        }
        catch {}
        let workUnitFPDDiv = createIfNotExists(simulationInfoDiv, "div", ".fpd");
        if (slot.queue_info.state !== "RUNNING") {
            if (slot.queue_info.state === "DOWNLOAD") {
                workUnitFPDDiv.innerHTML = "Downloading new work unit...";
            }
            else if (slot.queue_info.state === "SEND") {
                workUnitFPDDiv.innerHTML = "Uploading data...";
            }
            else if (slot.queue_info.state === "READY") {
                if (slot.simulation_info && slot.simulation_info.project > 0) {
                    workUnitFPDDiv.innerHTML = "Having trouble starting this work unit...";
                }
                else {
                    workUnitFPDDiv.innerHTML = "Waiting for new work unit...";
                }
            }
            else {
                let rawState = slot.queue_info.state || slot.status;
                let workUnitState = `${rawState.charAt(0) + rawState.substr(1).toLowerCase()}`;
                workUnitFPDDiv.innerHTML = `Current state: ${workUnitState}`;
            }
        }
        else {
            workUnitFPDDiv.innerHTML = `<strong>${workUnitFPD}</strong> frames per day <span>(${workUnitTPF} per frame)</span>`;
        }

        let timeInfoDiv = createIfNotExists(simulationInfoDiv, "div", ".time-info");

        let workUnitEtaSpanId = `#eta-${slot.id}`;
        let workUnitEtaSpan = createIfNotExists(timeInfoDiv, "span", workUnitEtaSpanId, "Not folding\xa0");
        if (yesUpdateProgress) {
            try {
                if (slot.queue_info.state !== "RUNNING") {
                    workUnitEtas[workUnitEtaSpanId] = 0;
                }
                else {
                    workUnitEtas[workUnitEtaSpanId] = slot.simulation_info.eta;
                }
            }
            catch {
                workUnitEtas[workUnitEtaSpanId] = 0;
            }
        }

        let workUnitDeadlineSpan = createIfNotExists(timeInfoDiv, "span", ".deadline");
        try {
            if (typeof slot.queue_info.timeremaining === "string" && (typeof workUnitEtas[workUnitEtaSpanId] !== "number" || workUnitEtas[workUnitEtaSpanId] > 0)) {
                workUnitDeadlineSpan.innerHTML = `(deadline: ${formatFAHTimeString(slot.queue_info.timeremaining)})`;
            }
            else {
                workUnitDeadlineSpan.innerHTML = "";
            }
        }
        catch {
            workUnitDeadlineSpan.innerHTML = "";
        }

        let workUnitCreditEstimate;
        if (typeof slot.queue_info.creditestimate === "string" && slot.queue_info.creditestimate !== "0") {
            workUnitCreditEstimate = parseInt(slot.queue_info.creditestimate).toLocaleString("en-US");
        }
        else {
            workUnitCreditEstimate = "N/A";
        }

        let workUnitCreditDiv = createIfNotExists(simulationInfoDiv, "div", ".credit");
        workUnitCreditDiv.innerHTML = `Credit estimate: ${workUnitCreditEstimate}`;

        if (slot.queue_info.basecredit !== "0") {
            let workUnitCreditBase = parseInt(slot.queue_info.basecredit).toLocaleString("en-US");
            workUnitCreditDiv.innerHTML += ` <span>(base: ${workUnitCreditBase})</span>`;
        }
    }

    let hardwareInfoDiv = createIfNotExists(additionalSlotInfo, "div", ".hardware-info");
    hardwareInfoDiv.innerHTML = "";

    let hardwareType;
    if (slot.description.includes("gpu:")) {
        hardwareType = "GPU";
    }
    else {
        hardwareType = "CPU";
    }

    if (nvml && slot.description.includes("gpu:") && Object.keys(slot.hardware_info).length === 0) {
        let gpuError = createDiv("GPU is lost! System reboot required.");
        gpuError.className = "error";
        hardwareInfoDiv.appendChild(gpuError);
        let bar = workUnitProgress.querySelector(".bar");
        bar.classList.remove("running");
        workUnitProgressPerSecond[`#${slotInfoBoxId} > .progress > .bar`] = "GPU_LOST";
        workUnitEtas[workUnitEtaSpanId] = 0;
    }

    let utilizations = [];
    if (typeof slot.hardware_info.utilization === "number") {
        let utilization;
        if (hardwareType === "GPU") {
            utilization = slot.hardware_info.utilization;
        }
        else {
            utilization = slot.hardware_info.utilization.toFixed(1);
        }
        utilizations.push(`utilization: ${utilization}%`);
    }
    if (slot.hardware_info.clock) {
        let letterC = "c";
        if (typeof slot.hardware_info.utilization === "number") {
            letterC = "C";
        }
        if (typeof slot.hardware_info.clock === "number") {
            utilizations.push(`${letterC}lock: ${slot.hardware_info.clock}MHz`);
        }
        else {
            let averageClock = Math.round(average(slot.hardware_info.clock));
            let clockTooltipContents = [];
            for (let i = 0; i < slot.hardware_info.clock.length; i++) {
                clockTooltipContents.push(`Processor ${i}: ${slot.hardware_info.clock[i].toFixed(3)}MHz`);
            }
            clockTooltipContents = clockTooltipContents.join("\n");
            utilizations.push(`${letterC}lock: <a title="${clockTooltipContents}" onclick="window.alert(this.title);">${averageClock}MHz</a>`);
        }
    }
    if (utilizations.length) {
        let workUnitUtilization = createDiv(`${hardwareType} ${utilizations.join(" | ")}`);
        hardwareInfoDiv.appendChild(workUnitUtilization);
    }

    if (slot.hardware_info.load_average && slot.hardware_info.load_average.length) {
        let workUnitLoadAverage = createDiv("Load average:");
        slot.hardware_info.load_average.forEach((load) => {
            workUnitLoadAverage.innerText += ` ${load.toFixed(2)}`;
        });
        hardwareInfoDiv.appendChild(workUnitLoadAverage);
    }

    if (typeof slot.hardware_info.memory_usage === "number" && typeof slot.hardware_info.memory_total === "number") {
        let usageAsPercent = Math.round(slot.hardware_info.memory_usage * 100 / slot.hardware_info.memory_total);
        let workUnitMemoryUtilization = createDiv(`Memory usage: ${slot.hardware_info.memory_usage}MiB / ${slot.hardware_info.memory_total}MiB (${usageAsPercent}%)`);
        hardwareInfoDiv.appendChild(workUnitMemoryUtilization);
    }

    let temperature = slot.hardware_info.temperature;
    if (temperature && typeof temperature === "object") {
        let mainTemperatureSensor = false;
        let tooltipContents = [];
        temperature.forEach((sensor) => {
            let sensorLabel = sensor[0];
            let sensorValue = sensor[1];
            if (!mainTemperatureSensor) {
                mainTemperatureSensor = sensorValue;
            }
            tooltipContents.push(`${sensorLabel}: ${sensorValue}°C`);
        });
        temperature = `<a title="${tooltipContents.join('\n')}" onclick="window.alert(this.title);">${mainTemperatureSensor}°C</a>`;
    }
    else if (typeof temperature === "number") {
        temperature = `${temperature}°C`;
    }
    if (typeof slot.hardware_info.fan_speed === "number" && temperature) {
        let workUnitTemperature = createDiv(`Fan speed: ${slot.hardware_info.fan_speed}% | <span>Temperature: ${temperature}</span>`);
        hardwareInfoDiv.appendChild(workUnitTemperature);
    }
    else if (typeof slot.hardware_info.fan_speed === "number" ) {
        let workUnitTemperature = createDiv(`Fan speed: ${slot.hardware_info.fan_speed}%`);
        hardwareInfoDiv.appendChild(workUnitTemperature);
    }
    else if (temperature) {
        let workUnitTemperature = createDiv(`Temperature: ${temperature}`);
        hardwareInfoDiv.appendChild(workUnitTemperature);
    }

    if (slot.hardware_info.temperature_hotspot) {
        let workUnitTemperatureHotspot = createDiv(`Hotspot: ${slot.hardware_info.temperature_hotspot}°C`);
        hardwareInfoDiv.appendChild(workUnitTemperatureHotspot);
    }

    if (slot.hardware_info.memory_temperature) {
        let workUnitMemoryTemperature = createDiv(`Memory temperature: ${slot.hardware_info.memory_temperature}°C`);
        hardwareInfoDiv.appendChild(workUnitMemoryTemperature);
    }

    if (typeof slot.hardware_info.power_usage === "number" && typeof slot.hardware_info.power_limit === "number") {
        let workUnitPowerUsage = createDiv(`Power usage: ${Math.round(slot.hardware_info.power_usage/1000)}W / ${Math.round(slot.hardware_info.power_limit/1000)}W`);
        hardwareInfoDiv.appendChild(workUnitPowerUsage);
    }
    else if (typeof slot.hardware_info.power_limit === "number") {
        let workUnitPowerLimit = createDiv(`Power limit: ${Math.round(slot.hardware_info.power_limit/1000)}W`);
        hardwareInfoDiv.appendChild(workUnitPowerLimit);
    }

    if (browserIsWebKit()) {
        slotInfoBox.style = `background-position: 0 ${-slotInfoDescription.offsetHeight}px; background-size: auto calc(100% + ${slotInfoDescription.offsetHeight}px);`;
    }

    return slotInfoBox;
}

function getStats() {
    let xhr = new XMLHttpRequest();
    xhr.onloadend = function() {
        updateStats(xhr);
    }
    xhr.responseType = "json";
    xhr.open("GET", "local-stats.json");
    xhr.send();
}

function setProjectInfo(info, element) {
    element.innerHTML = `<p>Disease Type: ${info.cause}</p>${info.description}<br><strong>This project is managed by ${info.manager} at ${info.institution}.</strong>`.replaceAll("\n", "<br>");
}

function getProjectInfo(projectId, element) {
    projectId = `${projectId}`;
    let cachedProjectInfo = projectInfoCache[projectId];
    if (cachedProjectInfo) {
        console.log("Project info loaded from cache.");
        setProjectInfo(cachedProjectInfo, element);
        return;
    }
    console.log("Getting project info from API.");
    element.innerHTML = "<p>Loading project info...</p>";
    let xhr = new XMLHttpRequest();
    xhr.onloadend = function() {
        if (xhr.status >= 400) {
            element.innerHTML = `<p>HTTP ${xhr.status}</p>`;
            return;
        }
        projectInfoCache[projectId] = xhr.response;
        setProjectInfo(xhr.response, element);
    }
    xhr.responseType = "json";
    xhr.open("GET", `project/${projectId}`);
    xhr.send();
}

function getAPIUserStats() {
    let xhr2 = new XMLHttpRequest();
    xhr2.onloadend = function() {
        updateAPIUserStats(xhr2);
    }
    xhr2.responseType = "json";
    xhr2.open("GET", "web-stats.json");
    xhr2.send();
}

function updateStats(xhr) {
    if (xhr.status != 200) {
        return;
    }
    if (lastUpdated === xhr.response.time_updated) {
        return;
    }
    lastUpdated = xhr.response.time_updated;
    document.getElementById("last-updated").innerText = `${lastUpdated.substr(11, 8)} UTC`;
    document.getElementById("last-updated").setAttribute("datetime", lastUpdated);

    if (!document.title.includes(xhr.response.donor)) {
        document.title = `Folding@home stats for ${xhr.response.donor}` 
    }
    document.getElementById("donor-link").innerText = xhr.response.donor;
    if (xhr.response.team) {
        showElement(document.getElementById("team-span"));
        document.getElementById("team-link").href = "https://stats.foldingathome.org/team/" + xhr.response.team;
        document.getElementById("team-link").innerText = xhr.response.team;
        if (xhr.response.eoc_id) {
            showElement(document.getElementById("eoc-span"));
            document.getElementById("team-link-eoc").href = "https://folding.extremeoverclocking.com/team_summary.php?s=&t=" + xhr.response.team;
        }
        else {
            hideElement(document.getElementById("eoc-span"));
        }
    }
    else {
        hideElement(document.getElementById("team-span"));
    }
    document.getElementById("ppd").innerText = Math.round(xhr.response.ppd).toLocaleString("en-US");
    let slotInfoContainer = document.getElementById("slot-info-container");
    xhr.response.slot_info.forEach((slot) => {
        try {
            let slotInfoBox = createSlotInfoBox(slotInfoContainer, slot, xhr.response.nvml);
        }
        catch (error) {
            console.error(error);
        }
    });
    if (!slotInfoContainerHeightSet) {
        slotInfoContainer.style.minHeight = `${slotInfoContainer.offsetHeight}px`;
        slotInfoContainerHeightSet = true;
    }
}

function updateAPIUserStats(xhr) { 
    if (xhr.status != 200) {
        return;
    }
    if (lastUpdated2 === xhr.response.time_updated) {
        return;
    }

    let userStats = xhr.response.user;
    let bonusStats = xhr.response.bonus;
    lastUpdated2 = userStats.time_updated;

    let donorSpan = document.getElementById("donor-span");
    let donorName = userStats.name;
    let eocDonorId = xhr.response.eoc_id;
    let donorId = userStats.id;
    if (donorName && eocDonorId) {
        showElement(donorSpan);
        let donorLink = document.getElementById("donor-link");
        donorLink.href = `https://folding.extremeoverclocking.com/user_summary.php?s=&u=${eocDonorId}`;
        donorLink.innerText = donorName;
    }
    else if (donorName && donorId) {
        showElement(donorSpan);
        let donorLink = document.getElementById("donor-link");
        donorLink.href = `https://stats.foldingathome.org/donor/${donorId}`;
        donorLink.innerText = donorName;
    }
    else {
        hideElement(donorSpan);
    }

    let workUnitsCompletedSpan = document.getElementById("work-units-completed-span");
    let workUnitsCompleted = userStats.wus;
    if (!workUnitsCompleted) {
        hideElement(workUnitsCompletedSpan);
    }
    else {
        showElement(workUnitsCompletedSpan);
        document.getElementById("work-units-completed").innerText = workUnitsCompleted.toLocaleString("en-US");
    }

    let scoreSpan = document.getElementById("score-span");
    let score = userStats.score;
    if (!score) {
        hideElement(scoreSpan);
    }
    else {
        showElement(scoreSpan);
        document.getElementById("score").innerText = score.toLocaleString("en-US");
    }

    let rankSpan = document.getElementById("rank-span");
    let rank = userStats.rank;
    if (!rank) {
        hideElement(rankSpan);
    }
    else {
        showElement(rankSpan);
        document.getElementById("rank").innerText = rank.toLocaleString("en-US");
    }

    let bonusSpan = document.getElementById("bonus-marker");
    let bonusActive = bonusStats.active;
    if (bonusActive) {
        showElement(bonusSpan);
    }
    else {
        hideElement(bonusSpan);
    }
}

});
