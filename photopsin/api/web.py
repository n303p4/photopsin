"""Get stats from folding@home web API.

Please run these sparingly to avoid pressure on the API.
"""

import json
import re
from urllib.parse import quote

import requests

CLEANHTML = re.compile('<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});')


def get_user_stats(name: str = None, *, endpoint: str = "https://api2.foldingathome.org/user/"):
    """Get stats regarding a particular user."""
    response = requests.get(f"{endpoint}{quote(name)}", timeout=5)
    if response.status_code >= 400:
        return
    response_json = response.json()
    return response_json


def get_project_info(project_id: int = None, *, endpoint: str = "https://api2.foldingathome.org/project/"):
    """Get project info."""
    response = requests.get(f"{endpoint}{quote(project_id)}", timeout=5)
    if response.status_code >= 400:
        return
    response_json = response.json()
    if isinstance(response_json.get("description"), str):
        response_json["description"] = re.sub(CLEANHTML, "", response_json["description"])
    return response_json


def get_bonus_stats(name: str = None, passkey: str = None, *, endpoint: str = "https://api2.foldingathome.org/bonus"):
    """Get bonus stats regarding a particular user."""
    params = {}
    if name:
        params["user"] = name
    if passkey:
        params["passkey"] = passkey
    response = requests.get(endpoint, params=params, timeout=5)
    if response.status_code >= 400:
        return
    response_json = json.loads(response.text)  # .json() doesn't like root-level arrays
    if response_json:
        return response_json[0]
    return response_json
