"""Get stats from FAHClient, NVML, and psutil."""

import ast
from datetime import datetime
import json
import subprocess
import telnetlib

try:
    import pynvml
except ImportError:  # Older versions, e.g. Ubuntu 18.04
    try:
        from py3nvml import py3nvml as pynvml
    except Exception:
        pynvml = False
try:
    import psutil
except ImportError:
    pass


def pci_decimal_to_hex(string: str):
    """Convert a decimal PCI bus/slot value into a hexadecimal one."""
    return hex(int(string))[2:].zfill(2)


def fahclient_command_pipe(telnet: telnetlib.Telnet, command: str):
    """Run an FAHClient command and return its output."""
    telnet.read_until(b"> ")
    telnet.write(bytes(command + "\n", encoding="ascii"))
    output = "\n".join(telnet.read_until(b"---", timeout=10).decode("ascii").split("\n")[2:-1])
    return output


def get_cpu_model():
    """On a Linux OS, get the machine's CPU model."""
    try:
        with open("/proc/cpuinfo") as cpu_info:
            for line in cpu_info.readlines():
                if line.startswith("model name"):
                    return line.split(":")[-1].strip()
    except Exception:
        pass


def get_cpu_clocks():
    """On a Linux OS, get the machine's CPU core clocks."""
    clocks = []
    try:
        with open("/proc/cpuinfo") as cpu_info:
            for line in cpu_info.readlines():
                if "mhz" in line.lower():
                    clocks.append(float(line.split(":")[-1].strip()))
    except Exception:
        pass
    return clocks


def memory_line_to_kib(line):
    return int(line.split(":")[-1].strip().split(" ")[0].strip())


def get_memory_usage():
    """On a Linux OS, get the machine's memory usage."""
    total = 0
    usage = 0
    try:
        with open("/proc/meminfo") as memory_info:
            for line in memory_info.readlines():
                if line.startswith("MemTotal"):
                    total = usage = memory_line_to_kib(line)
                if any(line.startswith(t) for t in ("MemFree", "Buffers", "Cached", "SReclaimable", "Shmem")):
                    usage -= memory_line_to_kib(line)
        return total, usage
    except Exception:
        pass
    return None, None


if hasattr(psutil, "getloadavg"):
    def get_load_average():
        return list(psutil.getloadavg())
else:
    def get_load_average():
        try:
            with open("/proc/loadavg") as file_object:
                return [float(l) for l in file_object.read().split()[:3]]
        except Exception:
            pass


def _get_stats(telnet: telnetlib.Telnet, *, cpu_model: str = None, raise_nvml_errors: bool = False):
    """Get FAH and GPU stats."""
    if pynvml:
        try:
            pynvml.nvmlInit()
        except pynvml.NVMLError_LibRmVersionMismatch as error:
            if raise_nvml_errors:
                raise error
        except Exception:
            pass
    options = ast.literal_eval(fahclient_command_pipe(telnet, "options"))
    stats = {
        "ppd": 0,
        "donor": options.get("user"),
        "team": options.get("team"),
        "passkey": options.get("passkey"),
        "nvml": bool(pynvml),
        "slot_info": ast.literal_eval(fahclient_command_pipe(telnet, "slot-info"))
    }
    queue_info = ast.literal_eval(fahclient_command_pipe(telnet, "queue-info"))
    try:
        sensors_temperatures = psutil.sensors_temperatures()
    except Exception:
        sensors_temperatures = {}
    for slot in stats["slot_info"]:
        if slot["status"] not in ("PAUSED", "DISABLED"):
            slot.setdefault("queue_info", {})
            try:
                slot["simulation_info"] = ast.literal_eval(fahclient_command_pipe(telnet, f'simulation-info {slot["id"]}'))
            except Exception:
                pass
            else:
                for unit in queue_info:
                    if slot["id"] == unit["slot"] \
                    and all(slot["simulation_info"][x] == unit[x] for x in ["project", "run", "clone", "gen"]):
                        slot["queue_info"] = unit
                        stats["ppd"] += int(unit["ppd"])
                        break
        slot["hardware_info"] = {}
        if "cpu:" in slot["description"]:
            if cpu_model:
                slot["description"] = f"{slot['description']} {cpu_model}"
            try:
                slot["hardware_info"]["utilization"] = psutil.cpu_percent()
            except Exception:
                pass
            try:
                slot["hardware_info"]["load_average"] = get_load_average()
            except Exception:
                pass
            try:
                temperatures = sensors_temperatures.get("k10temp", []) \
                             + sensors_temperatures.get("coretemp", [])
                temperature_label_value_pairs = []
                for sensor in temperatures:
                    label_value_pair = [sensor.label, round(sensor.current, 1)]
                    if label_value_pair[0] == "Tdie":
                        temperature_label_value_pairs.insert(0, label_value_pair)
                    else:
                        temperature_label_value_pairs.append(label_value_pair)
                slot["hardware_info"]["temperature"] = temperature_label_value_pairs
            except Exception:
                pass
            clocks = get_cpu_clocks()
            if clocks:
                slot["hardware_info"]["clock"] = clocks
            memory_total, memory_usage = get_memory_usage()
            if isinstance(memory_total, int) and isinstance(memory_usage, int):
                slot["hardware_info"]["memory_total"] = memory_total // 1024
                slot["hardware_info"]["memory_usage"] = memory_usage // 1024
        elif "gpu:" in slot["description"]:
            try:
                for sensor in sensors_temperatures.get("amdgpu", []):
                    if sensor.label == "edge":
                        slot["hardware_info"]["temperature"] = round(sensor.current, 1)
                    elif sensor.label == "junction":
                        slot["hardware_info"]["temperature_hotspot"] = round(sensor.current, 1)
                    elif sensor.label == "mem":
                        slot["hardware_info"]["memory_temperature"] = round(sensor.current, 1)
            except Exception:
                pass
            if not pynvml:
                continue
            try:
                pci_bus = pci_decimal_to_hex(slot["options"]["pci-bus"])
                pci_slot = pci_decimal_to_hex(slot["options"]["pci-slot"])
                pci_bus_id = bytes(f"0000:{pci_bus}:{pci_slot}.0", encoding="ascii")
                handle = pynvml.nvmlDeviceGetHandleByPciBusId(pci_bus_id)
            except Exception:
                continue
            try:
                utilization = pynvml.nvmlDeviceGetUtilizationRates(handle)
                slot["hardware_info"]["utilization"] = utilization.gpu
            except Exception:
                pass
            try:
                clock = pynvml.nvmlDeviceGetClockInfo(handle, pynvml.NVML_CLOCK_GRAPHICS)
                slot["hardware_info"]["clock"] = clock
            except Exception:
                pass
            try:
                memory_info = pynvml.nvmlDeviceGetMemoryInfo(handle)
                slot["hardware_info"]["memory_total"] = memory_info.total // 1048576
                slot["hardware_info"]["memory_usage"] = memory_info.used // 1048576
            except Exception:
                pass
            try:
                temperature = pynvml.nvmlDeviceGetTemperature(handle, pynvml.NVML_TEMPERATURE_GPU)
                slot["hardware_info"]["temperature"] = temperature
            except Exception:
                pass
            try:
                power_limit = pynvml.nvmlDeviceGetPowerManagementLimit(handle)
                slot["hardware_info"]["power_limit"] = power_limit
            except Exception:
                pass
            try:
                power_usage = pynvml.nvmlDeviceGetPowerUsage(handle)
                slot["hardware_info"]["power_usage"] = power_usage
            except Exception:
                pass
            try:
                fan_speed = pynvml.nvmlDeviceGetFanSpeed(handle)
                slot["hardware_info"]["fan_speed"] = fan_speed
            except Exception:
                pass
    stats["time_updated"] = datetime.utcnow().isoformat()
    return stats


def get_stats(*, cpu_model: str = None):
    """Main driver function."""
    with telnetlib.Telnet("localhost", port=36330) as telnet:
        return _get_stats(telnet, cpu_model=cpu_model)
